import React from 'react';

function App() {
  return <Frame1 {...frame1Data} />;
}

export default App;

function Frame1(props) {
  const {
    overlapGroup,
    anticovi,
    giPhnHi,
    thngTinDchT,
    bnDchT,
    tinTc,
    rectangle1,
    rectangle2,
    logIn,
    signUp,
    overlapGroup4,
    khaiBoYT,
    tinTc2,
    text1,
    phatHienNguocDoiVeTacDungPhuCuaVacX,
    vector1,
    vitNam,
    address,
    number,
    address2,
    number2,
    thGii,
    phone,
    phone2,
    phone3,
    phone4,
    frame3Props,
    frame3Props2,
  } = props;

  return (
    <div class="container-center-horizontal">
      <div className="frame-1 screen">
        <div className="overlap-group" style={{ backgroundImage: `url(${overlapGroup})` }}>
          <div className="overlap-group7">
            <h1 className="title roboto-bold-blue-whale-24px">{anticovi}</h1>
            <img
              className="vector"
              src="https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/vector@2x.svg"
            />
          </div>
          <div className="gi-phn-hi roboto-bold-blue-whale-18px">{giPhnHi}</div>
          <div className="thng-tin-dch-t roboto-bold-blue-whale-18px">{thngTinDchT}</div>
          <div className="bn-dch-t roboto-bold-blue-whale-18px">{bnDchT}</div>
          <div className="tin-tc roboto-bold-blue-whale-18px">{tinTc}</div>
          <div className="overlap-group6">
            <img className="rectangle-1" src={rectangle1} />
            <img className="rectangle-2" src={rectangle2} />
            <div className="log-in roboto-bold-onahau-18px">{logIn}</div>
            <div className="sign-up roboto-bold-onahau-18px">{signUp}</div>
          </div>
        </div>
        <div className="overlap-group4" style={{ backgroundImage: `url(${overlapGroup4})` }}>
          <div className="rectangle-10"></div>
          <div className="overlap-group8">
            <div className="khai-bo-y-t valign-text-middle">{khaiBoYT}</div>
          </div>
        </div>
        <div className="tin-tc-1">{tinTc2}</div>
        <div className="flex-row">
          <div className="overlap-group3">
            <div className="text-1 roboto-bold-green-blue-18px">{text1}</div>
            <img className="phat-hien-nguoc-xin-covid-19-1" src={phatHienNguocDoiVeTacDungPhuCuaVacX} />
          </div>
          <img className="vector-1" src={vector1} />
          <div className="flex-col">
            <div className="overlap-group1">
              <div className="rectangle-"></div>
              <div className="rectangle--1"></div>
              <div className="vit-nam valign-text-middle roboto-bold-white-18px-2">{vitNam}</div>
              <Frame3
                sCaNhim={frame3Props.sCaNhim}
                angIuTr={frame3Props.angIuTr}
                khi={frame3Props.khi}
                tVong={frame3Props.tVong}
              />
              <div className="address valign-text-middle roboto-bold-blue-whale-18px">{address}</div>
              <div className="number valign-text-middle roboto-bold-green-blue-18px">{number}</div>
              <div className="address-1 valign-text-middle roboto-bold-navy-blue-18px">{address2}</div>
              <div className="number-1 valign-text-middle roboto-bold-dodger-blue-18px">{number2}</div>
            </div>
            <div className="overlap-group2">
              <div className="rectangle-"></div>
              <div className="rectangle--1"></div>
              <div className="th-gii valign-text-middle roboto-bold-white-18px-2">{thGii}</div>
              <Frame3
                sCaNhim={frame3Props2.sCaNhim}
                angIuTr={frame3Props2.angIuTr}
                khi={frame3Props2.khi}
                tVong={frame3Props2.tVong}
              />
              <div className="phone valign-text-middle roboto-bold-blue-whale-18px">{phone}</div>
              <div className="phone-3 valign-text-middle roboto-bold-green-blue-18px">{phone2}</div>
              <div className="phone-1 valign-text-middle roboto-bold-navy-blue-18px">{phone3}</div>
              <div className="phone-2 valign-text-middle roboto-bold-dodger-blue-18px">{phone4}</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}


function Frame3(props) {
  const { sCaNhim, angIuTr, khi, tVong } = props;

  return (
    <div className="frame-3">
      <div className="overlap-group5">
        <div className="s-ca-nhim valign-text-middle roboto-bold-blue-whale-18px">{sCaNhim}</div>
        <div className="ang-iu-tr valign-text-middle roboto-bold-green-blue-18px">{angIuTr}</div>
        <div className="khi valign-text-middle roboto-bold-navy-blue-18px">{khi}</div>
        <div className="t-vong valign-text-middle roboto-bold-dodger-blue-18px">{tVong}</div>
      </div>
    </div>
  );
}

const frame3Data = {
    sCaNhim: "Số ca nhiễm",
    angIuTr: "Đang điều trị",
    khi: "Khỏi",
    tVong: "Tử vong",
};

const frame32Data = {
    sCaNhim: "Số ca nhiễm",
    angIuTr: "Đang điều trị",
    khi: "Khỏi",
    tVong: "Tử vong",
};

const frame1Data = {
    overlapGroup: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-4@1x.svg",
    anticovi: "AntiCoVi",
    giPhnHi: "Gửi phản hồi",
    thngTinDchT: "Thông tin dịch tễ",
    bnDchT: "Bản đồ dịch tễ",
    tinTc: "Tin tức",
    rectangle1: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-1@2x.svg",
    rectangle2: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/rectangle-2@2x.svg",
    logIn: "Log in",
    signUp: "Sign up",
    overlapGroup4: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/poster@1x.png",
    khaiBoYT: "KHAI BÁO Y TẾ",
    tinTc2: "TIN TỨC",
    text1: "Nghệ An: Chưa nhận được phản ứng nặng sau tiêm vắc-xin phòng COVID-19",
    phatHienNguocDoiVeTacDungPhuCuaVacX: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/phat-hien-nguoc-doi-ve-tac-dung-phu-cua-vac-xin-covid-19-1@2x.png",
    vector1: "https://anima-uploads.s3.amazonaws.com/projects/60812c070afd67277761228b/releases/60812c60d5fcd14c2857017a/img/vector-1@2x.svg",
    vitNam: "VIỆT NAM",
    address: "2 801",
    number: "272",
    address2: "2 490",
    number2: "35",
    thGii: "THẾ GIỚI",
    phone: "109 469 508",
    phone2: "25 341 731",
    phone3: "81 563 167",
    phone4: "2 413 158",
    frame3Props: frame3Data,
    frame3Props2: frame32Data,
};

